﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleMovingScript : MonoBehaviour
{
    public float xOffset, yOffset;
    public float lerpSpeed;
    Vector3 origPos, targetPos;

    // Start is called before the first frame update
    void Start()
    {
        origPos = transform.position;
        targetPos = new Vector3(origPos.x + xOffset, origPos.y +yOffset, origPos.z);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, targetPos, Time.deltaTime * lerpSpeed);
    }
}
