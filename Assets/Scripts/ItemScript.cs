﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemScript : MonoBehaviour
{
    public string type = "wheat";
    public int satiety, piety, order, health;

    SpriteRenderer rend;
    Sprite sprite;

    public GameObject ItemUI;
    public GameObject ItemParent;

    public Vector3 origPos;
    public float pingPongDistance;
    public float pingPongSpeed;

    float origTime;
    ParticleSystem particles;

    public Sprite wheat, rosary, shield, medicine;

    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<SpriteRenderer>();
        sprite = rend.sprite;
        ItemParent = GameObject.FindGameObjectWithTag("ItemParent");
        origPos = transform.position;
        origTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(transform.position.x, origPos.y + Mathf.PingPong((Time.time - origTime) * pingPongSpeed, pingPongSpeed), transform.position.z);
    }

    public void CreateItem(string newType, int newSatiety, int newPiety, int newOrder, int newHealth)
    {
        rend = GetComponent<SpriteRenderer>();
        type = newType;
        particles = GetComponentInChildren<ParticleSystem>();
        var main = particles.main;
        switch (type)
        {
            case "wheat":
                main.startColor = Color.yellow;
                rend.sprite = wheat;
                break;
            case "rosary":
                main.startColor = Color.magenta;
                rend.sprite = rosary;
                break;
            case "shield":
                main.startColor = Color.red;
                rend.sprite = shield;
                break;
            case "medicine":
                main.startColor = Color.blue;
                rend.sprite = medicine;
                break;
            default:
                rend.color = Color.white;
                break;
        }
        satiety = newSatiety;
        piety = newPiety;
        order = newOrder;
        health = newHealth;
    }

    private void OnMouseDown()
    {
        if (GameObject.FindGameObjectWithTag("ItemHandler").GetComponent<ItemHandlerScript>().currentObject == null)
        {
            PickUpItem();
        }
    }

    void PickUpItem()
    {
        Vector3 cameraPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        GameObject item = Instantiate(ItemUI, new Vector3(cameraPos.x, cameraPos.y, transform.position.z), transform.rotation, ItemParent.transform);
        item.GetComponent<ItemUIScript>().SetupItem(type, satiety, piety, order, health, rend.sprite);
        item.GetComponent<ItemUIScript>().isBeingHandled = true;
        item.transform.SetAsLastSibling();
        GameObject.FindGameObjectWithTag("ItemHandler").GetComponent<ItemHandlerScript>().currentObject = item;
        Destroy(this.gameObject);
    }
}
