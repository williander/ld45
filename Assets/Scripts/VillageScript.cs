﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.PostProcessing;
using UnityEngine.SceneManagement;
using TMPro;

public class VillageScript : MonoBehaviour
{
    public GameObject peasantNormie, peasantHungry, peasantFanatic, peasantRogue, peasantIll;

    public GameObject[] spawners;
    public GameObject villagersParent;

    public float generationDelay;
    public float verticalPositionClamp;

    public int satiety, piety, order, health = 0;
    public int crysisBorder = -100;
    public float weightToCreateSpecialPeasant = 0.5f;
    public bool debugMode = false;
    public GameObject[] debugTexts;

    float citizenAlpha = 1f;

    public int minParemeter = -100;
    public int maxParameter = 0;

    public int enterCrisisLine = -100;
    public int exitCrisisLine = -80;
    public float lerpSpeed = 5f;

    public bool isGenerating = true;
    bool inCrisis = false;

    public GameObject[] crises;
    public GameObject activeCurrentCrisis;

    public string deficitResource;
    public int minDefault = 5;
    public int maxDefault = 15;
    public int minDeficit = 10;
    public int maxDeficit = 20;

    public GameObject connectedSlot;
    public GameObject connectedCollider;
    public GameObject connectedCamera;

    public int hp = 2;

    CycleManagerScript cycleScript;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(GenerateVillagers());
        if (!debugMode)
        {
            debugTexts[0].GetComponent<TextMeshPro>().text = "";
            debugTexts[1].GetComponent<TextMeshPro>().text = "";
            debugTexts[2].GetComponent<TextMeshPro>().text = "";
            debugTexts[3].GetComponent<TextMeshPro>().text = "";
        }
    }

    // Update is called once per frame
    void Update()
    {
        cycleScript = GameObject.FindGameObjectWithTag("CycleManager").GetComponent<CycleManagerScript>();

        if (hp <= 0 || cycleScript.cycleNum >= cycleScript.cycleNumWin)
            isGenerating = false;

        satiety = Mathf.Clamp(satiety, minParemeter, maxParameter);
        piety = Mathf.Clamp(piety, minParemeter, maxParameter);
        order = Mathf.Clamp(order, minParemeter, maxParameter);
        health = Mathf.Clamp(health, minParemeter, maxParameter);

        if (debugMode)
        {
            debugTexts[0].GetComponent<TextMeshPro>().text = "Satiety = " + satiety.ToString();
            debugTexts[1].GetComponent<TextMeshPro>().text = "Piety = " + piety.ToString();
            debugTexts[2].GetComponent<TextMeshPro>().text = "Order = " + order.ToString();
            debugTexts[3].GetComponent<TextMeshPro>().text = "Health = " + health.ToString();
        }

        float negativeSum = Mathf.Clamp(satiety, minParemeter, 0) + Mathf.Clamp(piety, minParemeter, 0)
            + Mathf.Clamp(order, minParemeter, 0) + Mathf.Clamp(health, minParemeter, 0);

        if (negativeSum <= enterCrisisLine)
        {
            if (!inCrisis)
                StartCrisis();
        }

        if (negativeSum >= exitCrisisLine)
        {
            if (inCrisis)
                EndCrisis();
        }

        if (activeCurrentCrisis != null)
        {
            citizenAlpha = Mathf.Lerp(citizenAlpha, 0, Time.deltaTime * lerpSpeed);
            foreach (SpriteRenderer rend in activeCurrentCrisis.GetComponentsInChildren<SpriteRenderer>())

                rend.color = new Color(rend.color.r, rend.color.g, rend.color.b, 1 - citizenAlpha);
        }
        else
        {
            citizenAlpha = Mathf.Lerp(citizenAlpha, 1, Time.deltaTime * lerpSpeed);
        }

        foreach (GameObject obj in crises)
        {
            SpriteRenderer maskRend = obj.transform.GetChild(0).GetComponent<SpriteRenderer>();
            if (obj == activeCurrentCrisis)
            {
                foreach (SpriteRenderer rend in obj.GetComponentsInChildren<SpriteRenderer>())
                {
                    if (rend.name != "Mask")
                        rend.color = new Color(rend.color.r, rend.color.g, rend.color.b, 1 - citizenAlpha);
                }
                maskRend.color = new Color(maskRend.color.r, maskRend.color.g, maskRend.color.b, (1 - citizenAlpha) / 4);
            }
            else
            {
                foreach (SpriteRenderer rend in obj.GetComponentsInChildren<SpriteRenderer>())
                {
                    if (rend.name != "Mask")
                        rend.color = new Color(rend.color.r, rend.color.g, rend.color.b, Mathf.Lerp(rend.color.a, 0, Time.deltaTime * lerpSpeed));
                }
                    maskRend.color = new Color(maskRend.color.r, maskRend.color.g, maskRend.color.b, Mathf.Lerp(maskRend.color.a, 0, Time.deltaTime * lerpSpeed));
            }
        }

        foreach (SpriteRenderer rend in villagersParent.GetComponentsInChildren<SpriteRenderer>())
        {
            rend.color = new Color(rend.color.r, rend.color.g, rend.color.b, citizenAlpha);
        }
    }

    IEnumerator GenerateVillagers()
    {
        while (true)
        {
            if (isGenerating)
            {
                if (villagersParent.transform.childCount < 15)
                {
                    int movingDirection = Random.Range(0, 2);
                    
                    GameObject newVillager = Instantiate(AnalyzePeasants(), spawners[movingDirection].transform.position 
                        + new Vector3(0, Random.Range(-verticalPositionClamp, verticalPositionClamp)), transform.rotation, villagersParent.transform);
                    PeasantScript peasantScript = newVillager.GetComponent<PeasantScript>();
                    peasantScript.SetupMovementDirection(movingDirection);

                }
            }
            yield return new WaitForSeconds(generationDelay);
        }
    }

    GameObject AnalyzePeasants()
    {
        float negativeSum = Mathf.Clamp(satiety, minParemeter, 0) + Mathf.Clamp(piety, minParemeter, 0) 
            + Mathf.Clamp(order, minParemeter, 0) + Mathf.Clamp(health, minParemeter, 0);

        if (negativeSum != 0)
        {
            float satietyProportion = satiety / negativeSum;
            float pietyProportion = piety / negativeSum;
            float orderProportion = order / negativeSum;
            float healthProportion = health / negativeSum;

            int currentHungry = 0;
            int currentFanatic = 0;
            int currentRogue = 0;
            int currentIll = 0;
            foreach (PeasantScript scr in villagersParent.GetComponentsInChildren<PeasantScript>())
            {
                switch (scr.state)
                {
                    case "hungry":
                        currentHungry += 1;
                        break;
                    case "fanatic":
                        currentFanatic += 1;
                        break;
                    case "rogue":
                        currentRogue += 1;
                        break;
                    case "ill":
                        currentIll += 1;
                        break;
                    default:
                        break;
                }

            }

            float expectedSpoils = villagersParent.transform.childCount * negativeSum / minParemeter;

            float expectedHungryHumber = satietyProportion * expectedSpoils;
            float expectedFanaticNumber = pietyProportion * expectedSpoils;
            float expectedRogueNumber = orderProportion * expectedSpoils;
            float expectedIllNumber = healthProportion * expectedSpoils;

            float hungryPeasantPrediction = expectedHungryHumber - currentHungry;
            float fanaticPeasantPrediction = expectedFanaticNumber - currentFanatic;
            float roguePeasantPrediction = expectedRogueNumber - currentRogue;
            float illPeasantPrediction = expectedIllNumber - currentIll;

            float bestFit = Mathf.Max(hungryPeasantPrediction,
                fanaticPeasantPrediction,
                roguePeasantPrediction,
                illPeasantPrediction);

            if (bestFit > weightToCreateSpecialPeasant)
            {
                if (bestFit == hungryPeasantPrediction)
                    return peasantHungry;
                else if (bestFit == fanaticPeasantPrediction)
                    return peasantFanatic;
                else if (bestFit == roguePeasantPrediction)
                    return peasantRogue;
                else if (bestFit == illPeasantPrediction)
                    return peasantIll;
                else
                    return peasantNormie;
            }
            else
                return peasantNormie;
        }
        else
            return peasantNormie;
    }

    public void useItem(ItemUIScript item)
    {
        satiety = Mathf.Clamp(satiety + item.satiety, minParemeter, maxParameter);
        piety = Mathf.Clamp(piety + item.piety, minParemeter, maxParameter);
        order = Mathf.Clamp(order + item.order, minParemeter, maxParameter);
        health = Mathf.Clamp(health + item.health, minParemeter, maxParameter);
    }

    void StartCrisis()
    {
        float negativeSum = Mathf.Clamp(satiety, minParemeter, 0) + Mathf.Clamp(piety, minParemeter, 0)
            + Mathf.Clamp(order, minParemeter, 0) + Mathf.Clamp(health, minParemeter, 0);

        float minCrisis = Mathf.Min(satiety, piety, order, health);
        if (minCrisis == satiety)
        {
            activeCurrentCrisis = crises[0];
            satiety = (int) negativeSum;
            piety = 0;
            order = 0;
            health = 0;
        }
        else if (minCrisis == piety)
        {
            activeCurrentCrisis = crises[1];
            satiety = 0;
            piety = (int)negativeSum;
            order = 0;
            health = 0;
        }
        else if (minCrisis == order)
        {
            activeCurrentCrisis = crises[2];
            satiety = 0;
            piety = 0;
            order = (int)negativeSum;
            health = 0;
        }
        else if (minCrisis == health)

        {
            activeCurrentCrisis = crises[3];
            satiety = 0;
            piety = 0;
            order = 0;
            health = (int)negativeSum;
        }
        inCrisis = true;
    }

    void EndCrisis()
    {
        GameObject.FindGameObjectWithTag("CycleManager").GetComponent<CycleManagerScript>().ActiveCrisis = null;
        activeCurrentCrisis = null;
        inCrisis = false;
        Debug.Log("Crisis averted!");
    }

    public void ResourceDecline()
    {
        float negativeSum = Mathf.Clamp(satiety, minParemeter, 0) + Mathf.Clamp(piety, minParemeter, 0)
            + Mathf.Clamp(order, minParemeter, 0) + Mathf.Clamp(health, minParemeter, 0);

        if (activeCurrentCrisis != null)
        {
            DamageVillage();
        }

        if (deficitResource == "satiety")
            satiety -= Random.Range(minDeficit, maxDeficit);
        else
            satiety -= Random.Range(minDefault, maxDefault);
        if (deficitResource == "piety")
            piety -= Random.Range(minDeficit, maxDeficit);
        else
            piety -= Random.Range(minDefault, maxDefault);
        if (deficitResource == "order")
            order -= Random.Range(minDeficit, maxDeficit);
        else
            order -= Random.Range(minDefault, maxDefault);
        if (deficitResource == "health")
            health -= Random.Range(minDeficit, maxDeficit);
        else
            health -= Random.Range(minDefault, maxDefault);
    }

    void DamageVillage()
    {
        if (hp == 2)
        {
            connectedSlot.GetComponent<Collider2D>().enabled = false;
            connectedSlot.GetComponent<Image>().color = Color.red;
            if (connectedSlot.GetComponent<InventoryCellScript>().containingObject != null)
                Destroy(connectedSlot.GetComponent<InventoryCellScript>().containingObject);
        }
        else if (hp == 1)
        {
            connectedCollider.GetComponent<Collider2D>().enabled = false;
            connectedCamera.GetComponent<PostProcessingBehaviour>().enabled = true;
        }
        hp -= 1;

        bool readyToLose = true;
        foreach (GameObject obj in GameObject.FindGameObjectWithTag("CycleManager").GetComponent<CycleManagerScript>().villages)
        {
            if (obj.GetComponent<VillageScript>().hp > 0)
            {
                readyToLose = false;
            }
        }
        if (readyToLose)
            StartCoroutine(Lose());
    }

    IEnumerator Lose()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene("OutroLose");
    }
}
