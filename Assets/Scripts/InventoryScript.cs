﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryScript : MonoBehaviour
{
    public List<GameObject> inventoryCells = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        foreach (InventoryCellScript scr in transform.GetComponentsInChildren<InventoryCellScript>())
        {
            if (!inventoryCells.Contains(scr.gameObject))
                inventoryCells.Add(scr.gameObject);
        }
    }
}
