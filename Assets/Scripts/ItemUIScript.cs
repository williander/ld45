﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ItemUIScript : MonoBehaviour
{
    public string type = "";
    public int satiety, piety, order, health;

    public List<GameObject> collidedObjects = new List<GameObject>();
    public GameObject pairedCell;

    public bool isBeingHandled = false;

    public GameObject VFXRosary, VFXShield, VFXWheat, VFXMedicine;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isBeingHandled)
        {
            transform.position = Input.mousePosition;
        }

        if (Input.GetMouseButtonDown(0)
            && GameObject.FindGameObjectWithTag("ItemHandler").GetComponent<ItemHandlerScript>().currentObject == gameObject)
        {
            Place();
        }
    }

    public void SetupItem(string itemType, int newSatiety, int newPiety, int newOrder, int newHealth, Sprite spr)
    {
        type = itemType;
        satiety = newSatiety;
        piety = newPiety;
        order = newOrder;
        health = newHealth;
        GetComponent<Image>().sprite = spr;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collidedObjects.Contains(collision.gameObject))
            collidedObjects.Add(collision.gameObject);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collidedObjects.Contains(collision.gameObject))
            collidedObjects.Remove(collision.gameObject);
    }

    public void OnMouseDown()
    {
        if (!isBeingHandled && GameObject.FindGameObjectWithTag("ItemHandler").GetComponent<ItemHandlerScript>().currentObject == null)
        {
            isBeingHandled = true;
            if (pairedCell != null)
            {
                pairedCell.GetComponent<InventoryCellScript>().containingObject = null;
                pairedCell = null;
            }
            transform.SetAsLastSibling();
            StartCoroutine(GrabbingDelay());
        }
    }

    IEnumerator GrabbingDelay()
    {
        yield return new WaitForFixedUpdate();
        GameObject.FindGameObjectWithTag("ItemHandler").GetComponent<ItemHandlerScript>().currentObject = gameObject;
    }

    void Place()
    {
        if (collidedObjects.Count > 0)
        {
            GameObject closestCell = null;
            GameObject closestVillage = null;
            foreach(GameObject cell in collidedObjects)
            {
                if (cell.GetComponent<InventoryCellScript>() != null && cell.GetComponent<InventoryCellScript>().containingObject == null
                    && (closestCell == null || Vector3.Distance(cell.transform.position, transform.position) < Vector3.Distance(cell.transform.position, closestCell.transform.position)))
                    closestCell = cell;
                if (cell.GetComponent<VillageColliderScript>() != null
                    && (closestVillage == null || Vector3.Distance(cell.transform.position, transform.position) < Vector3.Distance(cell.transform.position, closestVillage.transform.position)))
                    closestVillage = cell;
            }
            if (closestCell != null)
            {
                closestCell.GetComponent<InventoryCellScript>().containingObject = gameObject;
                GameObject.FindGameObjectWithTag("ItemHandler").GetComponent<ItemHandlerScript>().currentObject = null;
                transform.position = closestCell.transform.position;
                isBeingHandled = false;
                pairedCell = closestCell;
            }
            else if (closestVillage != null)
            {
                isBeingHandled = false;
                closestVillage.GetComponent<VillageColliderScript>().connectedVillage.GetComponent<VillageScript>().useItem(this);
                /*switch (type)
                {
                    case "rosary":
                        Instantiate(VFXRosary, transform.position, transform.rotation);
                        break;
                    case "shield":
                        Instantiate(VFXShield, transform.position, transform.rotation);
                        break;
                    case "wheat":
                        Instantiate(VFXWheat, transform.position, transform.rotation);
                        break;
                    case "medicine":
                        Instantiate(VFXMedicine, transform.position, transform.rotation);
                        break;
                    case "default":
                        break;
                }*/
                Destroy(this.gameObject);
            }
        }
    }
}
