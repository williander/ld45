﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class CycleManagerScript : MonoBehaviour
{
    public int cycleNum = 1;
    public int cycleNumWin = 29;
    public bool DebugMode = false;
    public GameObject textMeshProObject;
    TextMeshPro textMesh;

    public float cycleTimeSeconds;
    public float currentCycle;
    public float timer;
    public float lerpSpeed = 5f;

    public GameObject ActiveCrisis;

    public GameObject[] villages;
    public GameObject[] sounds;

    public GameObject cycleRing;

    public GameObject pointer;

    public Sprite fullCounter;

    // Start is called before the first frame update
    void Start()
    {
        textMesh = textMeshProObject.GetComponent<TextMeshPro>();
    }

    // Update is called once per frame
    void Update()
    {
        if (timer >= 0)
            timer -= Time.deltaTime;
        else
        {
            currentCycle = cycleTimeSeconds * Random.Range(0.7f, 1.3f);
            timer = currentCycle;
            StartNextCycle();
        }

        if (cycleNum < cycleNumWin)
            cycleRing.GetComponent<Image>().fillAmount = 1 - timer / currentCycle;
        else
            cycleRing.GetComponent<Image>().fillAmount = 1;

        if (DebugMode)
            textMesh.text = string.Format(timer.ToString("0.00"));
        
        foreach (GameObject vil in villages)
        {
            if (ActiveCrisis == null && vil.GetComponent<VillageScript>().activeCurrentCrisis != null)
            {
                ActiveCrisis = vil.GetComponent<VillageScript>().activeCurrentCrisis;
            }
        }

        if (ActiveCrisis == null)
        {
            foreach (GameObject obj in sounds)
            {
                AudioSource src = obj.GetComponent<AudioSource>();
                src.volume = Mathf.Lerp(src.volume, 0, Time.deltaTime * lerpSpeed);
            }
        }
        else if (ActiveCrisis.name == "Hunger")
        {
            foreach (GameObject obj in sounds)
            {
                AudioSource src = obj.GetComponent<AudioSource>();
                if (obj == sounds[0])
                    src.volume = Mathf.Lerp(src.volume, 1, Time.deltaTime * lerpSpeed);
                else
                    src.volume = Mathf.Lerp(src.volume, 0, Time.deltaTime * lerpSpeed);
            }
        }
        else if (ActiveCrisis.name == "Fanatic")
        {
            foreach (GameObject obj in sounds)
            {
                AudioSource src = obj.GetComponent<AudioSource>();
                Debug.Log(ActiveCrisis.name);
                if (obj == sounds[1])
                    src.volume = Mathf.Lerp(src.volume, 1, Time.deltaTime * lerpSpeed);
                else
                    src.volume = Mathf.Lerp(src.volume, 0, Time.deltaTime * lerpSpeed);
            }
        }
        else if (ActiveCrisis.name == "Criminal")
        {
            foreach (GameObject obj in sounds)
            {
                AudioSource src = obj.GetComponent<AudioSource>();
                if (obj == sounds[2])
                    src.volume = Mathf.Lerp(src.volume, 1, Time.deltaTime * lerpSpeed);
                else
                    src.volume = Mathf.Lerp(src.volume, 0, Time.deltaTime * lerpSpeed);
            }
        }
        else if (ActiveCrisis.name == "Illness")
        {
            foreach (GameObject obj in sounds)
            {
                AudioSource src = obj.GetComponent<AudioSource>();
                if (obj == sounds[3])
                    src.volume = Mathf.Lerp(src.volume, 1, Time.deltaTime * lerpSpeed);
                else
                    src.volume = Mathf.Lerp(src.volume, 0, Time.deltaTime * lerpSpeed);
            }
        }
    }

    void StartNextCycle()
    {
        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Item"))
        {
            if (obj.GetComponent<ItemUIScript>() == null || (obj.GetComponent<ItemUIScript>().pairedCell == null && !obj.GetComponent<ItemUIScript>().isBeingHandled))
                Destroy(obj);
        }
        pointer.transform.GetChild(cycleNum - 1).GetComponent<Image>().sprite = fullCounter;
        cycleNum += 1;

        if (cycleNum < cycleNumWin)
        {
            foreach (GameObject obj in villages)
            {
                obj.GetComponent<VillageScript>().ResourceDecline();
            }
        }
        else
        {
            foreach (GameObject obj in villages)
            {
                obj.GetComponent<VillageScript>().isGenerating = false;
                obj.GetComponent<VillageScript>().activeCurrentCrisis = null;
                StartCoroutine(Win());
            }
        }
    }

    IEnumerator Win()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene("OutroWin");
    }
}
