﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingScript : MonoBehaviour
{
    public GameObject itemPrefab;
    public GameObject objectCreated;
    public VillageScript villageScr;
    
    public float timer;
    public float cyclesToCreate;

    CycleManagerScript cycleScript;

    public string type;
    public int satiety, piety, order, health;

    public bool isGenerating = false;

    // Start is called before the first frame update
    void Start()
    {
        cycleScript = GameObject.FindGameObjectWithTag("CycleManager").GetComponent<CycleManagerScript>();
        timer = cyclesToCreate * cycleScript.cycleTimeSeconds * Random.Range(0.5f, 1.5f);
        villageScr = GetComponentInParent<VillageScript>();
    }

    // Update is called once per frame
    void Update()
    {
        if (timer >= 0)
            timer -= Time.deltaTime;
        else if (cycleScript.cycleNum < cycleScript.cycleNumWin)
        {
            timer = cyclesToCreate * cycleScript.cycleTimeSeconds * Random.Range(0.5f, 1.5f);
            if (objectCreated == null && villageScr.hp > 0)
                CreateItem();
        }
    }

    void CreateItem()
    {
        GameObject createdObject = Instantiate(itemPrefab, transform.position, transform.rotation, GameObject.FindGameObjectWithTag("ItemParent").transform);
        objectCreated = createdObject;
        ItemScript scr = createdObject.GetComponent<ItemScript>();
        int expectedSatiety = (int)(satiety * Random.Range(0.8f, 1.2f));
        int expectedPiety = (int)(piety * Random.Range(0.8f, 1.2f));
        int expectedOrder = (int)(order * Random.Range(0.8f, 1.2f));
        int expectedHealth = (int)(health * Random.Range(0.8f, 1.2f));
        /*if (villageScr.activeCurrentCrisis != null)
        {
            expectedSatiety /= 2;
            expectedPiety /= 2;
            expectedOrder /= 2;
            expectedHealth /= 2;
        }*/
        scr.CreateItem(type, expectedSatiety, expectedPiety, expectedOrder, expectedHealth);
    }
}
