﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PeasantScript : MonoBehaviour
{
    public float movementSpeed = 1;
    public float disappearanceThreshold = 16f;

    public bool debugMode = false;

    int isMovingRight = 1;
    float origXPos;
    float currentColorH, currentColorS, currentColorV;

    SpriteRenderer rend;
    
    public string state = "";

    // Start is called before the first frame update
    void Start()
    {
        movementSpeed *= Random.Range(0.8f, 1.5f);
        rend = GetComponent<SpriteRenderer>();
        
        origXPos = transform.position.x;
    }

    public void SetupMovementDirection(int movingRight)
    {
        isMovingRight = movingRight;
        if (isMovingRight == 1)
            transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);

    }

    // Update is called once per frame
    void Update()
    {
        rend.sortingOrder = Mathf.RoundToInt(transform.position.y * 100f) * -1 + 1000;
        transform.position += new Vector3(Time.deltaTime * (isMovingRight == 1 ? -movementSpeed : movementSpeed), 0, 0);
        if (Mathf.Abs(transform.position.x - origXPos) > disappearanceThreshold)
            Destroy(this.gameObject);
    }
}
